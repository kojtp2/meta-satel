# SATEL BSP default providers

PREFERRED_PROVIDER_virtual/xserver = "xserver-xorg"
PREFERRED_PROVIDER_virtual/kernel ??= "linux-imx51satel"
PREFERRED_PROVIDER_u-boot ??= "u-boot-imx51satel"
PREFERRED_PROVIDER_virtual/bootloader ??= "u-boot-imx51satel"
