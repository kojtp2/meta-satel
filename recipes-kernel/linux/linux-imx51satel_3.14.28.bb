# Copyright (C) 2013-2015 Freescale Semiconductor
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Linux Kernel provided and supported by Freescale"
DESCRIPTION = "Linux Kernel provided and supported by Freescale with focus on \
i.MX Family Reference Boards. It includes support for many IPs such as GPU, VPU and IPU."

require recipes-kernel/linux/linux-imx.inc
require recipes-kernel/linux/linux-dtb.inc

DEPENDS += "lzop-native bc-native"

SRC_URI = "git://kojtp2@bitbucket.org/kojtp2/linux-imx51satel.git;protocol=https;branch=${SRCBRANCH} \
           file://defconfig \
"
SRCBRANCH = "master"
LOCALVERSION = "-satel"
SRCREV = "535bdeb9ec2ef1678f9c688451e157d66ed42edd"

# SRC_URI += " \
#    file://0001-ARM-imx6q-drop-unnecessary-semicolon.patch \
#    file://0002-ARM-clk-imx6q-fix-video-divider-for-rev-T0-1.0.patch \
#    file://0003-ARM-imx6sl-Disable-imx6sl-specific-code-when-imx6sl-.patch \
# "

COMPATIBLE_MACHINE = "(imx51satel)"
